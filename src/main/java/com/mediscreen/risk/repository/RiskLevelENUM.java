package com.mediscreen.risk.repository;

public enum RiskLevelENUM {
    NONE("None"),
    NOINFO("NO INFO"),
    EARLYONSET("EARLY ONSET"),
    BORDERLINE("BORDERLINE"),
    INDANGER("IN DANGER");

    private final String displayValue;

    private RiskLevelENUM(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
