package com.mediscreen.risk.repository;

public enum TriggerENUM {
    HEMOGLOBINEA1C("HEMOGLOBINE"),
    MICROALBUMINE("MICROALBUMINE"),
    TAILLE("TAILLE"),
    POIDS("POIDS"),
    FUMEUR("FUMEUR"),
    ANORMAL("ANORMAL"),
    CHOLESTEROLE("CHOLESTEROLE"),
    VERTIGE("VERTIGE"),
    RECHUTE("RECHUTE"),
    REACTION("REACTION"),
    ANTICORPS("ANTICORPS"),
    HEMOGLOBINA1C("HEMOGLOBINA1C"),
    MICROALBUMIN("MICROALBUMIN"),
    BODYHEIGHT("BODYHEIGHT"),
    BODYWEIGHT("BODYWEIGHT"),
    SMOKER("SMOKER"),
    ABNORMAL("ABNORMAL"),
    CHOLESTEROL("CHOLESTEROL"),
    DIZZINESS("DIZZINESS"),
    RELAPSE("RELAPSE"),
    ANTIBODIES("ANTIBODIES"),;

    private final String displayValue;

    private TriggerENUM(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
