package com.mediscreen.risk.service;

import com.mediscreen.risk.model.NoteModel;
import com.mediscreen.risk.model.PatientModel;
import com.mediscreen.risk.repository.RiskLevelENUM;
import com.mediscreen.risk.repository.TriggerENUM;
import com.mediscreen.risk.webclient.NoteWebClient;
import com.mediscreen.risk.webclient.PatientWebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RiskCalculator {

    @Autowired
    NoteWebClient noteWebClient;

    @Autowired
    PatientWebClient patientWebClient;

    public String getRisk(Integer patientId) {

        List<NoteModel> listNotes = noteWebClient.getAllNotesPatID(patientId);
        PatientModel patient = patientWebClient.getPatient(patientId);
        Integer patientAge = calculateAge(patient);
        String patientGender = patient.getSex();

        Integer nbOccurrencesTriggers = nbTriggers(listNotes);
        RiskLevelENUM patientRisk = RiskLevelENUM.NOINFO;

        //CONDITION STATEMENT FOR DETERMINING "NONE" RISK LEVEL
        if (nbOccurrencesTriggers < 2) {
            patientRisk = RiskLevelENUM.NONE;
        }
        //CONDITION STATEMENT FOR DETERMINING "BORDERLINE" RISK LEVEL
        else if (nbOccurrencesTriggers == 2 && patientAge >= 30) {
            patientRisk = RiskLevelENUM.BORDERLINE;
        }
        //CONDITION STATEMENTS FOR DETERMINING "INDANGER" RISK LEVEL
        else if (2 <= nbOccurrencesTriggers && nbOccurrencesTriggers <= 3 && patientAge < 30 && patientGender.equals("M")) {
            patientRisk = RiskLevelENUM.INDANGER;
        } else if (2 <= nbOccurrencesTriggers && nbOccurrencesTriggers <= 4 && patientAge < 30 && patientGender.equals("F")) {
            patientRisk = RiskLevelENUM.INDANGER;
        } else if (2 <= nbOccurrencesTriggers && nbOccurrencesTriggers <= 6 && patientAge >= 30) {
            patientRisk = RiskLevelENUM.INDANGER;
        }
        //CONDITION STATEMENT FOR DETERMINING "EARLYONSET" RISK LEVEL
        else if (3 <= nbOccurrencesTriggers && nbOccurrencesTriggers <= 5 && patientAge < 30 && patientGender.equals("M")) {
            patientRisk = RiskLevelENUM.EARLYONSET;
        } else if (3 <= nbOccurrencesTriggers && nbOccurrencesTriggers <= 7 && patientAge < 30 && patientGender.equals("F")) {
            patientRisk = RiskLevelENUM.EARLYONSET;
        } else if (nbOccurrencesTriggers >= 7 && patientAge >= 30) {
            patientRisk = RiskLevelENUM.EARLYONSET;
        }

        return String.format("Patient: %s %s (age %s) diabete assessement is %s",
                patient.getGiven(),
                patient.getFamily(),
                patientAge,
                patientRisk
        );
    }

    public int nbTriggers(List<NoteModel> listNotesModel) {
        AtomicInteger nbOccurrences = new AtomicInteger();

        List<TriggerENUM> listTriggers = new ArrayList<TriggerENUM> (Arrays.asList(TriggerENUM.values()));

        for (NoteModel note: listNotesModel) {
            String noteString = note.getE().toUpperCase().replaceAll(" ", "");

            listTriggers.forEach(trigger -> {
                if (noteString.contains(trigger.toString())) {
                    nbOccurrences.getAndIncrement();
                }
            });
        }

        return nbOccurrences.get();
    }

    public int calculateAge(PatientModel patient) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
        String birthDate = patient.getDob();
        if (birthDate != null) {
            LocalDate birthday = LocalDate.parse(birthDate, formatter);
            LocalDate today = LocalDate.now(); //Today's date

            int age = Period.between(birthday, today).getYears();
            return age;
        }
        else {
            return 0;
        }
    }


}
