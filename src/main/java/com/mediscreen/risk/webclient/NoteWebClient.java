package com.mediscreen.risk.webclient;

import com.mediscreen.risk.model.NoteModel;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public class NoteWebClient {
    private final String BASE_URL_LOCALHOST = "http://localhost:8082";

    //private final String BASE_URL_LOCALHOST = "http://notes:8081";
    // Declare the path for patient list
    private final String PATH_NOTE_LIST = "/patHistory/list";
    //Declare the PatientId parameter name to use in the WebClient request
    private final String PAT_ID = "?patId=";

    public List<NoteModel> getAllNotesPatID(int patId) {
        Flux<NoteModel> getNoteList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + PATH_NOTE_LIST + PAT_ID + patId)
                .retrieve()
                .bodyToFlux(NoteModel.class);
        List<NoteModel> noteList = getNoteList.collectList().block();
        return noteList;
    }
}
