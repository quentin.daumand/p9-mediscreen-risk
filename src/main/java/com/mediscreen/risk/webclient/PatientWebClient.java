package com.mediscreen.risk.webclient;

import com.mediscreen.risk.model.PatientModel;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class PatientWebClient {
    private final String BASE_URL_LOCALHOST = "http://localhost:8081";

    //private final String BASE_URL_LOCALHOST = "http://patients:8081";
    private final String PATH_PATIENTS_LIST = "/patients/json";
    private final String PATH_PATIENT = "/patient/json";
    //Declare the AttractionId name to use in the request of the Rest Template Web Client
    private final String USER_ID = "?id=";
    private final String PATH_PATIENT_EXIST = "/check/patient/";

    public List<PatientModel> getListPatients() {
        Flux<PatientModel> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + PATH_PATIENTS_LIST)
                .retrieve()
                .bodyToFlux(PatientModel.class);
        List<PatientModel> patientList = getPatientList.collectList().block();
        return patientList;
    }

    public PatientModel getPatient(Integer patId) {
        Mono<PatientModel> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + PATH_PATIENT + USER_ID + patId)
                .retrieve()
                .bodyToMono(PatientModel.class);
        PatientModel patientList = getPatientList.block();
        return patientList;
    }

    public boolean checkPatientIdExist(int patientId) {
        Mono<Boolean> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + PATH_PATIENT_EXIST + patientId)
                .retrieve()
                .bodyToMono(Boolean.class);
        boolean patientExists = getPatientList.block();
        return patientExists;
    }

    public boolean checkPatientFamilyExist(String family) {
        System.out.println(BASE_URL_LOCALHOST + "/check/familyPatient/" + family);
        Mono<Boolean> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + "/check/familyPatient/" + family)
                .retrieve()
                .bodyToMono(Boolean.class);
        boolean patientExists = getPatientList.block();
        return patientExists;
    }

    public PatientModel patientFamilyExist(String familyName) {
        Mono<PatientModel> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + "/patient/family/json" + "?family=" + familyName)
                .retrieve()
                .bodyToMono(PatientModel.class);
        PatientModel patientList = getPatientList.block();
        return patientList;
    }
}
