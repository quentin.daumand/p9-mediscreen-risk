package com.mediscreen.risk.controller;

import com.mediscreen.risk.webclient.PatientWebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PatientController {

    private final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    PatientWebClient patientWebClient;

    @GetMapping("/patients")
    public String patientList(Model model) {
        model.addAttribute("patients", patientWebClient.getListPatients());
        LOGGER.info("GET /patients : OK");
        return "patients";
    }
}