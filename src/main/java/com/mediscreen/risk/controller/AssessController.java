package com.mediscreen.risk.controller;

import com.mediscreen.risk.model.NoteModel;
import com.mediscreen.risk.model.PatientModel;
import com.mediscreen.risk.repository.RiskLevelENUM;
import com.mediscreen.risk.service.RiskCalculator;
import com.mediscreen.risk.webclient.NoteWebClient;
import com.mediscreen.risk.webclient.PatientWebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AssessController {
    private final Logger LOGGER = LoggerFactory.getLogger(AssessController.class);
    @Autowired
    PatientWebClient patientWebClient;

    @Autowired
    RiskCalculator riskCalculator;

    @PostMapping("/assess/id")
    public String assessId(Integer patId)  {
        if (!patientWebClient.checkPatientIdExist(patId)) {
            LOGGER.info("GET /assess/familyName : Non existent familyName");
            return "redirect:patients";
        }

        return riskCalculator.getRisk(patId);
    }

    @PostMapping("/assess/familyName")
    public String assessFamilyName(String familyName)  {
        if (!patientWebClient.checkPatientFamilyExist(familyName)) {
            LOGGER.info("GET /assess/familyName : Non existent familyName");
            return "redirect:patients";
        }

        LOGGER.info("GET /assess/familyName: OK");
        PatientModel patient = patientWebClient.patientFamilyExist(familyName);
        return riskCalculator.getRisk(patient.getId());

    }
}
