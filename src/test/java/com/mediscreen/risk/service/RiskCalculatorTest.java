package com.mediscreen.risk.service;

import com.mediscreen.risk.model.NoteModel;
import com.mediscreen.risk.model.PatientModel;
import com.mediscreen.risk.webclient.NoteWebClient;
import com.mediscreen.risk.webclient.PatientWebClient;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doReturn;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RiskCalculatorTest {

    @Autowired
    RiskCalculator riskCalculator;

    @MockBean
    PatientWebClient patientWebClient;

    @MockBean
    NoteWebClient noteWebClient;

    public PatientModel Sophie() {
        PatientModel Sophie = new PatientModel();
        Sophie.setId(1);
        Sophie.setGiven("Sophie");
        Sophie.setFamily("Princess");
        Sophie.setDob("1994-07-10");
        Sophie.setSex("F");
        Sophie.setAddress("1 rue du Paradis");
        Sophie.setEmailAddress("princessSophie@mail.fr");
        Sophie.setPhone("0102030405");
        return Sophie;
    }

    public NoteModel noteModel1() {
        LocalDateTime date = LocalDateTime.of(2021,01,01,13,45,30);

        NoteModel noteModel1 = new NoteModel();
        noteModel1.setId("1245567");
        noteModel1.setPatId("1");
        noteModel1.setCreationDateTime(date);
        noteModel1.setE("Antibodies");
        return noteModel1;
    }

    public NoteModel noteModel2() {
        LocalDateTime date = LocalDateTime.of(2021,01,01,13,45,30);

        NoteModel noteModel1 = new NoteModel();
        noteModel1.setId("1245567");
        noteModel1.setPatId("1");
        noteModel1.setCreationDateTime(date);
        noteModel1.setE("Antibodies Antibodies");
        return noteModel1;
    }

    @Test
    public void getRiskNONE(){
        List<NoteModel> listReportModel = new ArrayList<>();
        listReportModel.add(noteModel1());
        doReturn(listReportModel)
                .when(noteWebClient)
                .getAllNotesPatID(Sophie().getId());

        doReturn(Sophie())
                .when(patientWebClient)
                .getPatient(Sophie().getId());

        Assert.assertTrue(riskCalculator.getRisk(Sophie().getId()).contains("NONE"));
    }

}
