FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /usr/app
COPY build/libs/p9-mediscreen-risk-1.0-SNAPSHOT.jar mediscreen-risk.jar
EXPOSE 8080
CMD ["java", "-jar", "mediscreen-risk.jar"]